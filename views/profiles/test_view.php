<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profiles */
/* @var $form yii\widgets\ActiveForm */

$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Check Test';
?>
<div class="profiles-update">

    <h1><?= Html::encode($testData->title) ?></h1>
    <div class="profiles-form col-md-5">

        <?php foreach($testData->questions as $key => $questionItem): ?>
            <label for=""><?php echo $questionItem->question;?></label>

            <div class="input-group">
                <span class="input-group-addon">
                    <input type="checkbox" aria-label="...">
                </span>
                <input type="text" class="form-control" aria-label="..." value="<?php echo $answers[$key];?>">
            </div> <br>
        <?php endforeach; ?>

    </div>

</div>
