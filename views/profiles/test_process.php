<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profiles */
/* @var $form yii\widgets\ActiveForm */

$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $testData->title];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profiles-update">

    <h3><?= Html::encode($testData->title) ?></h3>
    <div class="profiles-form col-md-5 row">
        <?php $form = ActiveForm::begin(['action' => Url::to(['submit-test', 'id' => $model->id]), 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <?php foreach($testData->questions as $key => $questionItem): ?>
            <div class="form-group">
                <label for=""><?php echo $questionItem->question;?></label>
                <input type="text" name="Questions[answers][]" class="form-control" value="">
            </div>
        <?php endforeach; ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
