<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profiles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Profiles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'name',
                'format'=>'raw',
                'value'=> function($model){
                    return Html::a($model->name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'department',
                'format' => 'raw',
                'value' => function($model){
                    return "<span class='label label-success'>{$model->department}</span>";
                }
            ],
            [
                'attribute' => 'level',
                'format' => 'raw',
                'value' => function($model){
                    return "<span class='label label-info'>{$model->levels[$model->level]}</span>";
                }
            ],
            [
                'attribute' => 'level',
                'value' => function($model){
                    return $model->levels[$model->level];
                }
            ],
             'last_attestation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
