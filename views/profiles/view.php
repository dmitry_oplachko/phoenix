<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Profiles */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profiles-view">

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            Rate: <span class="label label-info"><?php echo round($rate);?>%</span>
        </div>
        <div class="panel-body">
            <div class="col-md-9">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'department',
                            'format' => 'raw',
                            'value' => "<span class='label label-success'>{$model->department}</span>"
                        ],
                        'last_attestation',
                    ],
                ]) ?>
            </div>
            <div class="col-md-3">
                <div class="list-group">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'list-group-item']) ?>
                    <?= Html::a('Тест Сотрудника', ['test', 'id' => $model->id], ['class' => 'list-group-item']) ?>
                    <?= Html::a('Тест TeamLead', ['test-lead', 'id' => $model->id], ['class' => 'list-group-item']) ?>
                    <?= Html::a('Тест Project Manager\'a', ['test-pm', 'id' => $model->id], ['class' => 'list-group-item']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php foreach($skillCategories->categories as $key => $skillCategory):?>
        <?php if($skillCategory->skills[0]->title != ''):?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $skillCategory->title;?></h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>
                                Title
                            </th>
                            <th>
                                Level
                            </th>
                            <th>
                                Experience
                            </th>
                        </tr>
                        <?php foreach($skillCategory->skills as $key => $skill):?>
                                <tr>
                                    <td class="col-md-3">
                                        <span class='label label-info'><?php echo $skill->title;?></span>
                                    </td>
                                    <td class="col-md-1">
                                        <?php echo $skill->level;?>
                                    </td>
                                    <td class="col-md-1">
                                        <?php echo $skill->experience;?>
                                    </td>
                                </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            </div>
        <?php endif;?>
    <?php endforeach;?>

    <?php
    $statisticCounts = [];
    $statisticHours = [];

    foreach ($statisticData as $key => $statisticItem) {
        $statisticCounts[] = [
            'name' => $statisticItem['tracker'],
            'y' => (integer)$statisticItem['count'],
            'color' => new JsExpression("Highcharts.getOptions().colors[{$key}]") //set color
        ];
    }

    foreach ($statisticData as $key => $statisticItem) {
        $statisticHours[] = [
            'name' => $statisticItem['tracker'],
            'y' => (integer)$statisticItem['spent_time'],
            'color' => new JsExpression("Highcharts.getOptions().colors[{$key}]") //set color
        ];
    }

    ?>

    <?php echo Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => 'Графики',
            ],
            'labels' => [
                'items' => [
                    [
                        'html' => 'Соотношение в количестве',
                        'style' => [
                            'left' => '115px',
                            'top' => '0px',
                            'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                        ],
                    ],
                    [
                        'html' => 'Соотношение в часах',
                        'style' => [
                            'left' => '815px',
                            'top' => '0px',
                            'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                        ],
                    ],
                ],
            ],
            'series' => [
                [
                    'type' => 'pie',
                    'name' => 'Шт.',
                    'data' => $statisticCounts,
                    'center' => [190, 150],
                    'size' => 300,
                    'showInLegend' => false,
                    'dataLabels' => [
                        'enabled' => true,
                    ],
                ],
                [
                    'type' => 'pie',
                    'name' => 'Часов',
                    'data' => $statisticHours,
                    'center' => [850, 150],
                    'size' => 300,
                    'showInLegend' => false,
                    'dataLabels' => [
                        'enabled' => true,
                    ],
                ],
            ],
        ]
    ]);?>



    <table class="table table-hovered table-striped table-bordered">
    <?php foreach($statisticData as $statisticItem):?>
        <tr>
            <td>
                <b><?php echo $statisticItem['tracker'];?></b>
            </td>
            <td>
                <?php echo $statisticItem['count'];?> (<?php echo $statisticItem['spent_time'];?> часов)
            </td>
        </tr>
    <?php endforeach; ?>
    </table>


        <h2>View Attestations</h2>
        <div class="list-group col-md-5">
            <?php foreach($attestations as $attestation): ?>
                <a href="<?php echo Url::to(['test-view', 'id' => $attestation->id]);?>" class="list-group-item"><b><?php echo $attestation->title; ?></b> [<?php echo $attestation->date;?>]</a>
            <?php endforeach;?>
        </div>

</div>
