<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profiles */

$this->title = 'Update Profiles: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profiles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="profiles-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Profile</h3>
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'department')->dropDownList($departments) ?>

                <?= $form->field($model, 'level')->dropDownList($levels) ?>

                <?= $form->field($model, 'rate')->textInput() ?>

                <?= $form->field($model, 'last_attestation')->textInput() ?>
            </div>
        </div>

        <?php foreach($skillCategories->categories as $key => $skillCategory):?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $skillCategory->title;?> [Вес: <?php echo $skillCategory->weight;?>]</h3>
                </div>
                <div class="panel-body">
                    <?php foreach($skillCategory->skills as $key => $skill):?>
                        <div class="skills">
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="text" name="Profiles[categories][<?php echo $skillCategory->title;?>][<?php echo $key;?>][title]" data-id="<?php echo $key;?>" data-first="Profiles[categories][<?php echo $skillCategory->title;?>][" data-last='][title]' placeholder="Title" class="form-control col-md-3" value="<?php echo $skill->title;?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="Profiles[categories][<?php echo $skillCategory->title;?>][<?php echo $key;?>][level]" data-id="<?php echo $key;?>" data-first="Profiles[categories][<?php echo $skillCategory->title;?>][" data-last="][level]" placeholder="Level [1-10]" class="form-control col-md-1" value="<?php echo $skill->level;?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="Profiles[categories][<?php echo $skillCategory->title;?>][<?php echo $key;?>][experience]" data-id="<?php echo $key;?>" data-first="Profiles[categories][<?php echo $skillCategory->title;?>][" data-last="][experience]" placeholder="Experience" class="form-control col-md-1" value="<?php echo $skill->experience;?>">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="Profiles[categories][<?php echo $skillCategory->title;?>][<?php echo $key;?>][weight]" data-id="<?php echo $key;?>" data-first="Profiles[categories][<?php echo $skillCategory->title;?>][" data-last="][weight]" value="<?php echo $skillCategory->weight;?>">
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                    <div class="form-group">
                        <input type="button" class="btn btn-success" name="name" onclick="addNew(this)" value="Add">
                    </div>

                </div>
            </div>
        <?php endforeach;?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>

<script>
function addNew(elem){
    var formInline = jQuery(elem).parent().prev().find('.form-inline').last();
    var currentContainer = jQuery(formInline).parent();
    var clonedForm = jQuery(formInline).clone();
    var inputs = jQuery(clonedForm).find('input[type=text]');
    jQuery.each(inputs, function(key, val){
        jQuery(val).attr('name', jQuery(val).data('first')+(jQuery(val).data('id')+1)+jQuery(val).data('last'));
        jQuery(val).attr('data-id', jQuery(val).data('id')+1);
        jQuery(val).val('');
    });
    jQuery(currentContainer).append(clonedForm);
}
</script>

<style media="screen">
    .form-inline {
        margin-bottom: 15px;
    }
</style>
