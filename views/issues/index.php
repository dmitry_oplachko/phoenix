<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use miloschuman\highcharts\Highcharts;

use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issues-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            // 'tracker',
            // 'status',
            // 'priority',
            [
                'attribute' => 'Task',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->subject, ['/issues/update', 'id' => $model->id]);
                }
            ],
            // 'author',
            'profile.name:text:Исполнитель',
            // 'updated',
            // 'category',
            // 'target_version',
            // 'start_date',
            // 'due_date',
            [
                'attribute' => 'Оценили',
                'value' => 'estimated_time',
                'pageSummary' => true
            ],
            [
                'attribute' => 'Реально',
                'value' => 'spent_time',
                'pageSummary' => true
            ],
            [
                'attribute' => 'Запас',
                'format' => 'raw',
                'value' => function ($model) {
                        return $model->estimated_time - $model->spent_time;
                },
                'pageSummary' => true
            ],
            // 'done:text:Done %',
            // 'created',
            // 'closed',
            // 'related_issues',
            // 'private',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
        'showPageSummary' => true
    ]); ?>

<?php
$statisticCounts = [];
$statisticHours = [];

foreach ($statisticData as $key => $statisticItem) {
    $statisticCounts[] = [
        'name' => $statisticItem['tracker'],
        'y' => (integer)$statisticItem['count'],
        'color' => new JsExpression("Highcharts.getOptions().colors[{$key}]") //set color
    ];
}

foreach ($statisticData as $key => $statisticItem) {
    $statisticHours[] = [
        'name' => $statisticItem['tracker'],
        'y' => (integer)$statisticItem['spent_time'],
        'color' => new JsExpression("Highcharts.getOptions().colors[{$key}]") //set color
    ];
}

?>

<?php echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Графики',
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'Соотношение в количестве',
                    'style' => [
                        'left' => '115px',
                        'top' => '0px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
                [
                    'html' => 'Соотношение в часах',
                    'style' => [
                        'left' => '815px',
                        'top' => '0px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            [
                'type' => 'pie',
                'name' => 'Шт.',
                'data' => $statisticCounts,
                'center' => [190, 180],
                'size' => 300,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => true,
                ],
            ],
            [
                'type' => 'pie',
                'name' => 'Часов',
                'data' => $statisticHours,
                'center' => [850, 180],
                'size' => 300,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => true,
                ],
            ],
        ],
    ]
]);?>



<table class="table table-hovered table-striped table-bordered">
<?php foreach($statisticData as $statisticItem):?>
    <tr>
        <td>
            <b><?php echo $statisticItem['tracker'];?></b>
        </td>
        <td>
            <?php echo $statisticItem['count'];?> (<?php echo $statisticItem['spent_time'];?> часов)
        </td>
    </tr>
<?php endforeach; ?>
</table>

</div>
