<?php

namespace app\models;

use Yii;
use ruskid\csvimporter\csvimporter;
use ruskid\csvimporter\csvreader;
use ruskid\csvimporter\arimportstrategy;
/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $name
 * @property string $start_date
 */
class Projects extends \yii\db\ActiveRecord
{

    public $csvFile;
    public $i = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['start_date'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['csvFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_date' => 'Start Date',
        ];
    }

    public function import($data){
        $project = Projects::findOne(['name' => $data[1]]);

        if(!$project) { //if project not exist create new one
            $project = new Projects([
                'name' => $data[1]
            ]);
            $project->save();
        }

        if($data[8]===''){
            $data[8] = 'No profile';
        }

        $profile = Profiles::findOne(['name' => $data[8]]);
        if(!$profile){
            $profile = new Profiles([
                'name' => $data[8],
                'department' => $data[10]
            ]);
            $profile->save();
        }

        $issue = new Issues([ //create new issue and assign them to project
            'project_id' => $project->id,
            'tracker' => $data[2],
            'status' => $data[4],
            'priority' => $data[5],
            'subject' => $data[6],
            'author' => $data[7],
            'assignee' => $profile->id,
            'start_date' => $data[12],
            'due_date' => $data[13],
            'estimated_time' => $data[14],
            'spent_time' => $data[15],
            'done' => $data[16],
            'created' => $data[17],
            'closed' => $data[18]
        ]);

        if($issue->save()){
            return true;
        }
    }

    public function importTimeLog($data){
        $project = Projects::findOne(['name' => $data[0]]);

        if(!$project) { //if project not exist create new one
            $project = new Projects([
                'name' => $data[0]
            ]);
            $project->save();
        }

        if($data[2]===''){
            $data[2] = 'No profile';
        }

        $profile = Profiles::findOne(['name' => $data[2]]);
        if(!$profile){
            $profile = new Profiles([
                'name' => $data[2],
                'department' => $data[3]
            ]);
            $profile->save();
        }

        $issue = new Issues([ //create new issue and assign them to project
            'project_id' => $project->id,
            'tracker' => $data[4],
            'status' => '',
            'priority' => '',
            'subject' => $data[5],
            'author' => '',
            'assignee' => $profile->id,
            'start_date' => '',
            'due_date' => '',
            'estimated_time' => '',
            'spent_time' => $data[6],
            'done' => '',
            'created' => $data[1],
            'closed' => ''
        ]);

        if($issue->save()){
            return true;
        }
    }
}
