<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "issues".
 *
 * @property integer $id
 * @property string $project
 * @property string $tracker
 * @property string $parent_task
 * @property string $status
 * @property string $priority
 * @property string $subject
 * @property string $author
 * @property string $assignee
 * @property string $updated
 * @property string $category
 * @property string $target_version
 * @property string $start_date
 * @property string $due_date
 * @property string $estimated_time
 * @property string $spent_time
 * @property integer $done
 * @property string $created
 * @property string $closed
 * @property string $related_issues
 * @property string $private
 */
class Issues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issues';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['spent_time'], 'number'],
            [['assignee', 'done'], 'integer'],
            [['project', 'parent_task'], 'string', 'max' => 3],
            [['tracker'], 'string', 'max' => 6],
            [['status', 'category', 'estimated_time'], 'string', 'max' => 11],
            [['priority'], 'string', 'max' => 9],
            [['subject'], 'string', 'max' => 72],
            [['author'], 'string', 'max' => 20],
            [['updated', 'created'], 'string', 'max' => 16],
            [['target_version', 'start_date', 'due_date', 'closed', 'related_issues'], 'string', 'max' => 10],
            [['private'], 'string', 'max' => 2]
        ];
    }

    public function getProfile(){
        return $this->hasOne(Profiles::className(), ['id' => 'assignee']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project' => 'Project',
            'tracker' => 'Tracker',
            'parent_task' => 'Parent Task',
            'status' => 'Status',
            'priority' => 'Priority',
            'subject' => 'Subject',
            'author' => 'Author',
            'assignee' => 'Assignee',
            'updated' => 'Updated',
            'category' => 'Category',
            'target_version' => 'Target Version',
            'start_date' => 'Start Date',
            'due_date' => 'Due Date',
            'estimated_time' => 'Estimated Time',
            'spent_time' => 'Spent Time',
            'done' => 'Done',
            'created' => 'Created',
            'closed' => 'Closed',
            'related_issues' => 'Related Issues',
            'private' => 'Private',
        ];
    }
}
