<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property integer $id
 * @property string $name
 * @property string $department
 * @property integer $rate
 * @property string $last_attestation
 */
class Profiles extends \yii\db\ActiveRecord
{

    public $levels = [
        'juniour',
        'middle',
        'seniour'
    ];

    public $departments = [
        '',
        'Development',
        'Web',
        'Desktop',
        'Mobile',
        'QA',
        'Design'
    ];

    public $questionsFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['rate', 'level'], 'integer'],
            [['last_attestation'], 'safe'],
            [['name', 'department'], 'string', 'max' => 127],
            [['cv'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'department' => 'Department',
            'rate' => 'Rate',
            'last_attestation' => 'Last Attestation',
        ];
    }
}
