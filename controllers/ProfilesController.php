<?php

namespace app\controllers;

use Yii;
use app\models\Profiles;
use app\models\Questions;
use app\models\Issues;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * ProfilesController implements the CRUD actions for Profiles model.
 */
class ProfilesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Profiles::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $attestationsList = Questions::findAll(['profile_id' => $id]);
        $query = "SELECT id, tracker, COUNT(id) AS count, SUM(spent_time) AS spent_time FROM issues WHERE assignee = {$id} GROUP BY tracker";
        $statisticData = Yii::$app->db->createCommand($query)->queryAll();
        $skillCategories = file_get_contents('../web/json/skillCategories.json');
        if($skillCategories!=''){
            $skillCategories = json_decode($skillCategories);
            if($model->cv!=''){
                $loadedCV = json_decode($model->cv);
                $skillCategories->categories = $loadedCV;
            }

            $skills = [];
            //Phase #1
            $skillIndex = 0; // Sk
            $skillSum = 0;
            $maxExp = 0;
            foreach($skillCategories->categories as $category){
                foreach($category->skills as $skill){
                    if($skill->title != ''){
                        $skills[] = $skill;
                        $skillSum = $skillSum + $skill->level;
                        if($skill->experience > $maxExp){
                            $maxExp = $skill->experience;
                        }
                    }
                }
            }
            if(count($skills)>0){
                $skillIndex = ($skillSum)/(10*count($skills));
            }

            //Phase #2

            $pmTest = Questions::find()->where(['profile_id' => $id, 'type' => '3'])->one();
            $estPm = 0;
            if($pmTest && $pmTest->answers){
                $pmAnswers = json_decode($pmTest->answers);
                $pmSum = 0;

                foreach($pmAnswers as $answer){
                    $pmSum = $pmSum + $answer;
                }
                if(count($pmAnswers)>0){
                    $estPm = $pmSum/(10*count($pmAnswers));
                }
            }

            $tlTest = Questions::find()->where(['profile_id' => $id, 'type' => '1'])->one();
            $estTl = 0;
            if($tlTest && $tlTest->answers){
                $tlAnswers = json_decode($tlTest->answers);
                $tlSum = 0;
                foreach($tlAnswers as $answer){
                    $tlSum = $tlSum + $answer;
                }
                if(count($tlAnswers)>0){
                    $estTl = $tlSum/(10*count($tlAnswers));
                }
            }

            $quality = 0;

            $connection = \Yii::$app->db;
            $issuesTotalSum = $connection->createCommand("SELECT SUM(spent_time) FROM issues WHERE assignee = {$id} AND MONTH(start_date) = MONTH(NOW())");
            $issuesTotalSum = $issuesTotalSum->queryScalar();

            $issuesTaskSum = $connection->createCommand("SELECT SUM(spent_time) FROM issues WHERE assignee = {$id} AND tracker = 'Ticket' AND MONTH(start_date) = MONTH(NOW())");
            $issuesTaskSum = $issuesTaskSum->queryScalar();

            if($issuesTotalSum != 0 && $issuesTaskSum != 0){
                $quality = $issuesTotalSum/$issuesTaskSum;
            }

            $estim = 0;

            $estimatesTotalSum = $connection->createCommand("SELECT SUM(estimated_time) FROM issues WHERE assignee = {$id}");
            $estimatesTotalSum = $estimatesTotalSum->queryScalar();

            $estimatesSpentSum = $connection->createCommand("SELECT SUM(spent_time) FROM issues WHERE assignee = {$id}");
            $estimatesSpentSum = $estimatesSpentSum->queryScalar();

            if($estimatesTotalSum != 0 && $estimatesSpentSum != 0){
                $estim = $estimatesTotalSum/$estimatesSpentSum;
            }

            $exp = 0;
            if($maxExp != 0){
                $exp = $maxExp/7;
            }

            $weight = json_decode(file_get_contents('../web/json/weights.json'));
            $rate = 0;
            if($weight->v1!=0 && $skillIndex!=0 && $weight->v2!=0 && $exp !=0){
                $rate = $weight->u1 * $estPm + $weight->u2 * $estTl + $weight->u3 * $quality + $weight->u4 * $estim;
                $rate = $rate / ($weight->v1 * $skillIndex + $weight->v2 * $exp);
                $rate = $rate*100;
            }


            return $this->render('view', [
                'model' => $model,
                'attestations' => $attestationsList,
                'statisticData' => $statisticData,
                'skillCategories' => $skillCategories,
                'rate' => $rate
            ]);
        } else {
            var_dump('skillCategories.json not found');
        }

    }

    /**
     * Creates a new Profiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profiles();
        $departments = array_combine($model->departments, $model->departments);
        $levels = $model->levels;

        $skillCategories = file_get_contents('../web/json/skillCategories.json');
        if($skillCategories!=''){
            $skillCategories = json_decode($skillCategories);
            $model->last_attestation = new \yii\db\Expression('NOW()');
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'departments' => $departments,
                    'levels' => $levels,
                    'skillCategories' => $skillCategories
                ]);
            }
        } else {
            var_dump('Error');
        }

    }

    public function actionTest($id)
    {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post())){
            $modelQuestions = new Questions();
            $questions = file_get_contents(UploadedFile::getInstance($model, 'questionsFile')->tempName);

            if($questions != ''){
                $testData = json_decode($questions);
                $modelQuestions->questions = $questions;
                $modelQuestions->profile_id = $id;
                $modelQuestions->title = $testData->title;
                $modelQuestions->date = new \yii\db\Expression('NOW()');

                if($modelQuestions->save()){
                    return $this->render('test_process', [
                        'model' => $modelQuestions,
                        'testData' => $testData,
                    ]);
                } else {
                    return var_dump('Error', $modelQuestions->errors);
                }

            }
        } else {
            return $this->render('upload_questions', [
                'model' => $model,
            ]);
        }
    }

    public function actionTestLead($id)
    {
        $model = $this->findModel($id);

        $modelQuestions = new Questions();
        $questions = file_get_contents('../web/json/lead.json');

        if($questions != ''){
            $testData = json_decode($questions);
            $modelQuestions->questions = $questions;
            $modelQuestions->profile_id = $id;
            $modelQuestions->title = $testData->title;
            $modelQuestions->type = 1;
            $modelQuestions->date = new \yii\db\Expression('NOW()');

            if($modelQuestions->save()){
                return $this->render('test_process', [
                    'model' => $modelQuestions,
                    'testData' => $testData,
                ]);
            } else {
                return var_dump('Error', $modelQuestions->errors);
            }
        }
    }

    public function actionTestPm($id)
    {
        $model = $this->findModel($id);
        $modelQuestions = new Questions();
        $questions = file_get_contents('../web/json/pm.json');

        if($questions != ''){
            $testData = json_decode($questions);
            $modelQuestions->questions = $questions;
            $modelQuestions->profile_id = $id;
            $modelQuestions->title = $testData->title;
            $modelQuestions->type = 3;
            $modelQuestions->date = new \yii\db\Expression('NOW()');

            if($modelQuestions->save()){
                return $this->render('test_process', [
                    'model' => $modelQuestions,
                    'testData' => $testData,
                ]);
            } else {
                return var_dump('Error', $modelQuestions->errors);
            }

        }
    }

    public function actionSubmitTest($id){
        $model = Questions::findOne(['id' => $id]);
        $post = Yii::$app->request->post();
        if($model->load($post)){
            $model->answers = json_encode($post['Questions']['answers']);
            if($model->save()){
                $profile = $this->findModel($model->profile_id);
                $profile->last_attestation = new \yii\db\Expression('NOW()');
                if($profile->save()){
                    return $this->redirect(['view', 'id' => $model->profile_id]);
                } else {
                    return var_dump('Error', $model->errors);
                }
            } else {
                return var_dump('Error', $model->errors);
            }
        }
    }

    public function actionTestView($id){
        $model = Questions::findOne(['id' => $id]);
        $testData = json_decode($model->questions);
        $answers = json_decode($model->answers);
        return $this->render('test_view', [
            'answers' => $answers,
            'testData' => $testData,
        ]);
    }

    /**
     * Updates an existing Profiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $departments = array_combine($model->departments, $model->departments);
        $levels = $model->levels;

        $skillCategories = file_get_contents('../web/json/skillCategories.json');
        if($skillCategories!=''){
            $skillCategories = json_decode($skillCategories);
            if($model->cv!=''){
                $loadedCV = json_decode($model->cv);
                $skillCategories->categories = $loadedCV;
            }

            if ($model->load(Yii::$app->request->post())) {
                $categoriesData = [];
                foreach(Yii::$app->request->post()['Profiles']['categories'] as $key => $category){
                    $newCategory = \Yii::createObject('stdClass');
                    $newCategory->title = $key;
                    $newCategory->skills = $category;
                    $newCategory->weight = $category[0]['weight'];
                    $categoriesData[] = $newCategory;
                }
                $model->cv = json_encode($categoriesData);
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'departments' => $departments,
                    'levels' => $levels,
                    'skillCategories' => $skillCategories
                ]);
            }
        }
    }

    /**
     * Deletes an existing Profiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
