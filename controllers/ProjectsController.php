<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Issues;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Projects::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Issues::find()->where(['project_id' => $id]),
        ]);
        $query = "SELECT id, tracker, COUNT(id) AS count, SUM(spent_time) AS spent_time FROM issues WHERE project_id = {$id} GROUP BY tracker";
        $statisticData = Yii::$app->db->createCommand($query)->queryAll();

        return $this->render('/issues/index', [
            'dataProvider' => $dataProvider,
            'statisticData' => $statisticData
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post())){
            $model->importIssues(UploadedFile::getInstance($model, 'csvFile'));

            if ($model->save()) {
                $model->importIssues(UploadedFile::getInstance($model, 'csvFile'));

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionImport()
    {
        $model = new Projects();
        if(Yii::$app->request->post()){
            ini_set('auto_detect_line_endings',TRUE);
            $handle = fopen(UploadedFile::getInstance($model, 'csvFile')->tempName,'r');
            $iterator = 0;
            while ( ($data = fgetcsv($handle) ) !== FALSE ) {
                if($iterator!=0){ // skip first line
                    $model->import($data);
                }
                $iterator++;
            }
            ini_set('auto_detect_line_endings',FALSE);
            return $this->redirect(['index']);
        } else {
            return $this->render('import', [
                'model' => $model,
            ]);
        }
    }

    public function actionImportTimelog()
    {
        $model = new Projects();
        if(Yii::$app->request->post()){
            ini_set('auto_detect_line_endings',TRUE);
            $handle = fopen(UploadedFile::getInstance($model, 'csvFile')->tempName,'r');
            $iterator = 0;
            while ( ($data = fgetcsv($handle) ) !== FALSE ) {
                if($iterator!=0){ // skip first line
                    $model->importTimelog($data);
                }
                $iterator++;
            }
            ini_set('auto_detect_line_endings',FALSE);
            return $this->redirect(['index']);
        } else {
            return $this->render('import', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
